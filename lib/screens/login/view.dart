import 'package:flutter/material.dart';
import 'package:project/screens/bottom_navigation/view.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  Widget _inputField({String hint, bool secure, Widget icon}) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 50, vertical: 30),
      child: Directionality(
        textDirection: TextDirection.rtl,
        child: TextFormField(
          obscureText: secure,
          decoration: InputDecoration(
              suffixIcon: icon,
              hintText: hint,
              hintStyle: TextStyle(fontSize: 17),
              filled: true,
              fillColor: Colors.white,
              border:
                  OutlineInputBorder(borderRadius: BorderRadius.circular(15))),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blue,
      body: ListView(
        children: [
          SizedBox(
            height: 150,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                'تسجيل الدخول',
                style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontSize: 20),
              )
            ],
          ),
          _inputField(hint: 'الرقم القومي', secure: false),
          _inputField(
              hint: 'كلمه السر',
              secure: true,
              icon: Icon(Icons.remove_red_eye)),
          Padding(
            padding: const EdgeInsets.symmetric(
              horizontal: 50,
            ),
            child: Row(
              children: [
                Text(
                  'نسيت كلمه السر؟',
                  style: TextStyle(color: Colors.white),
                )
              ],
            ),
          ),
          InkWell(
            onTap: () {
              Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(
                      builder: (context) => BottomNavigationScreen()));
            },
            child: Container(
              width: 200,
              height: 50,
              margin: EdgeInsets.symmetric(horizontal: 120, vertical: 30),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10), color: Colors.green),
              child: Center(
                child: Text(
                  'دخول',
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 20,
                      fontWeight: FontWeight.bold),
                ),
              ),
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                'حساب جديد',
                style: TextStyle(color: Colors.black),
              ),
              SizedBox(
                width: 15,
              ),
              Text(
                'ليس لديك حساب؟',
                style: TextStyle(color: Colors.white),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
