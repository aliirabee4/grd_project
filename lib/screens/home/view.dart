import 'package:flutter/material.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  List<String> _dataList = ['متابعه اونلاين', 'تقيم دكتور'];
  GlobalKey<ScaffoldState> _globalKey = GlobalKey<ScaffoldState>();
  Widget _item({String title}) {
    return Container(
      margin: EdgeInsets.only(right: 20, top: 10, left: 20),
      width: MediaQuery.of(context).size.width,
      height: 50,

      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(10),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Icon(Icons.keyboard_arrow_down),
          SizedBox(
            width: 20,
          ),
          Text(title),
        ],
      ),
    );
  }
  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
        key: _globalKey,
        backgroundColor: Colors.grey.withOpacity(0.1),
        drawer: Drawer(
          child: ListView(
            children: [
              CircleAvatar(
                backgroundColor: Colors.black,
                radius: 50,
              ),
              SizedBox(height: 20,),
              Row(mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text('Ayaat Mohamed')
              ],),
              _item(title: 'حسابي'),
              _item(title: 'الخدمات'),
              _item(title: 'الاساله الشاىعه'),
              _item(title: 'مركز المساعده'),
              _item(title: 'ثواصل معنا'),
            ],
          ),
        ),
        appBar: AppBar(
          backgroundColor: Colors.white,
          actions: [
            IconButton(
                icon: Icon(
                  Icons.menu,
                  color: Colors.black,
                ),
                onPressed: () {
                  _globalKey.currentState.openDrawer();
                })
          ],
          leading: Icon(
            Icons.notifications,
            color: Colors.black,
          ),
        ),
        body: Column(
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 50, vertical: 15),
              child: TextFormField(
                decoration: InputDecoration(
                    contentPadding: EdgeInsets.all(5),
                    border: OutlineInputBorder(borderRadius: BorderRadius.circular(10)),
                    prefixIcon: Icon(Icons.search),
                    hintText: 'بحث عن خدمه'),
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(
                  height: 50,
                ),
                Text(
                  'الخدمات الطبيه',
                  style: TextStyle(color: Colors.blue, fontSize: 18, fontWeight: FontWeight.bold),
                )
              ],
            ),
            SizedBox(
              height: 10,
            ),
            SizedBox(
              height: 140,
              child: ListView.builder(
                  itemCount: 10,
                  scrollDirection: Axis.horizontal,
                  itemBuilder: (ctx, index) {
                    return Container(
                      width: 130,
                      margin: EdgeInsets.symmetric(horizontal: 10),
                      height: 140,
                      decoration: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.circular(10)),
                      child: Column(
                        children: [
                          SizedBox(
                            height: 10,
                          ),
                          Icon(Icons.margin),
                          Text(
                            'خدمات طبيه',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          SizedBox(
                            height: 5,
                          ),
                          Text('بحث عن دكتور '),
                          Text('حجز عند دكتور '),
                          Text('اعاده كشف '),
                          SizedBox(
                            height: 10,
                          ),
                          Container(
                            width: 80,
                            height: 20,
                            decoration: BoxDecoration(borderRadius: BorderRadius.circular(10), color: Colors.green),
                            child: Center(
                              child: Text(
                                'مشاهده المزيد',
                                style: TextStyle(color: Colors.white),
                              ),
                            ),
                          )
                        ],
                      ),
                    );
                  }),
            ),
            SizedBox(
              height: 30,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(
                  height: 50,
                ),
                Text(
                  'الخدمات الاكثر طلبا',
                  style: TextStyle(color: Colors.blue, fontSize: 18, fontWeight: FontWeight.bold),
                )
              ],
            ),
            SizedBox(
              height: 30,
            ),
            SizedBox(
              height: 140,
              child: ListView.builder(
                  itemCount: _dataList.length,
                  scrollDirection: Axis.horizontal,
                  itemBuilder: (ctx, index) {
                    return Container(
                      width: 100,
                      margin: EdgeInsets.symmetric(horizontal: 10),
                      height: 140,
                      decoration: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.circular(10)),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            _dataList[index],
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Icon(
                            Icons.add,
                            size: 50,
                          ),
                        ],
                      ),
                    );
                  }),
            )
          ],
        ),
      ),
    );
    ;
  }
}
